#include <stdio.h>

int main(void){
  float x;

  printf("Enter a floating-point number to be printed in several formats: ");
  fflush(stdout);
  scanf("%f", &x);

  // exponential; left-justified; field size of 8; 1 digit after the decimal point
  printf("%-8.1e\n", x);
  // exponential; right-justified; field size of 10; 6 digits after the decimal point
  printf("%10.6e\n", x);
  // fixed decimal; left-justified; field size of 8; 3 digits after the decimal point
  printf("%-8.3f\n", x);
  // fixed decimal; right-justified; field size of 6; 0 digits after the decimal point
  printf("%6.0f\n", x);

  return 0;
}
