#include <stdio.h>

int main(void){
  int num, day, month, year;
  float price;

  printf("Enter item number: ");
  fflush(stdout);
  scanf("%d", &num);

  printf("Enter unit price: ");
  fflush(stdout);
  scanf("%f", &price);

  printf("Enter purchase date (dd/mm/yyyy): ");
  fflush(stdout);
  scanf("%d/%d/%d", &day, &month, &year);

  printf("Item\t\tUnit\t\t\tPurchase\n\t\tPrice\t\t\tDate\n%d\t\t$%6.2f\t\t\t%.2d/%.2d/%.4d\n", num, price, day, month, year);

  return 0;
}
