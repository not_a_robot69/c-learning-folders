#include <stdio.h>

int main(void){
  float loan, interest, monthly;

  printf("Enter amount of loan: ");
  fflush(stdout);
  scanf("%f", &loan);

  printf("Enter interest rate: ");
  fflush(stdout);
  scanf("%f", &interest);

  printf("Enter monthly payment: ");
  fflush(stdout);
  scanf("%f", &monthly);

  return 0;
}
