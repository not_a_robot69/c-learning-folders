#include <stdio.h>
#define PI 3.141592

int main(void){
  printf("Volume of a sphere with a 10-metre radius: %.2f\n", 4.0f/3.0f * PI * 10 * 10 *10);

  return 0;
}
