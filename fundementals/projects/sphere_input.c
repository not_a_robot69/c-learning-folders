#include <stdio.h>
#define PI 3.141592

int main(void){
  float r;

  printf("Enter a radius: ");
  fflush(stdout);
  scanf("%f", &r);
  printf("Volume of a sphere with a %.2f metre radius: %.2f\n", r, 4.0f/3.0f * PI * r * r *r);

  return 0;
}
