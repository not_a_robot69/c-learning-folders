#include <stdio.h>

int main(void){
  float x;

  printf("Enter a value for x: ");
  fflush(stdout);
  scanf("%f", &x);

  printf("The value of ((((3x+2)x-5)x-1)x+7)x-6 is: %.3f\n", ((((3 * x + 2) * x - 5) * x - 1) * x + 7) * x - 6);

  return 0;
}
