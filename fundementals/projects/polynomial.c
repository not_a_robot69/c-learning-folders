#include <stdio.h>

int main(void){
  float x;

  printf("Enter a value for x: ");
  fflush(stdout);
  scanf("%f", &x);

  printf("The value of 3x^5 + 2x^4 - 5x^3 - x^2 + 7x - 6 is: %.3f\n", (3 * (x * x * x * x * x)) + (2 * (x * x * x * x)) - (5 * (x * x * x)) - (x * x) + (7 * x) - 6);

  return 0;
}
