#include <stdio.h>

#define INCHES_PER_POUND 166  //#define is what is known as a macro definition. Just like #include, it is a preprocessing directive, so every call of INCHES_PER_POUND will be replaced with 166.

// this program computes the dimensional width of a box from user input
int main(void){
  int height, length, width, volume, weight;

  printf("Enter height of the box: ");
  fflush(stdout);
  scanf("%d", &height); //scanf works like printf, except it's taking input from stdin instead of printing to stdout
  printf("Enter length of the box: ");
  fflush(stdout);
  scanf("%d", &length);
  printf("Enter width of the box: ");
  fflush(stdout);
  scanf("%d", &width);

  volume = height * length * width;
  // add 165 so it rounds up no matter what, since `/` truncates decimals by default
  weight = (volume + INCHES_PER_POUND - 1) / INCHES_PER_POUND;

  printf("Volume (cubic inches): %d\n", volume);
  printf("Dimensional weight (pounds): %d\n", weight);

  return 0;
}
