#include <stdio.h>  //directive

int main(void){
  printf("Parkinson's Law:\nWork expands so as to "); //statement
  fflush(stdout); //statement
  printf("fill the time\n");  //statement
  printf("available for completion.\n");  //statement

  return 0; //statement
}
