.TL
Notes on Chapter 4: Expressions

.NH
Differences in dividing between C89 and C99
.PP
In C89, if either operand of `/` are negative, the results of division can be rounded either up or down (e.g the true value of -9 / 7 is -1.285..., but in C89 it can either be -1 or -2).
 In C99, the result of the division is always truncated towards zero (-9 / 7 = -1, always).
.PP
In C89, if either `i` or `j` is negative, the sign of `i % j` depends on the implementation (e.g `-9 % 7` could be either -2 or 5).
 In C99, the value of `i % j` has the same sign as i (so -9 % 7 is always -2).
